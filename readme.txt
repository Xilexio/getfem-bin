getfem++
LGPL license

Contains getfem++ and its dependencies:
* getfem++ 4.2 (LGPL)
* gmm (included in getfem++ 4.2)
* lapack 3.4.1
* blas
* muparser 2.2.3 (MIT)
* qhull 2012.1 (with modified headers, BSD-like)

Compiled on win 7 with:
* GNUStep's mingw with g++ 4.6.1

=== COMPILATION ===
blas and lapack win32 binaries for mingw downloaded from http://icl.cs.utk.edu/lapack-for-windows/lapack/

built muparser with:
cd build
mingw32-make -f makefile.mingw SHARED=1
and later manually created library (didn't work with getfem++ otherwise):
cd build/obj/gcc_shared_rel
ar cru libmuparser.a muParser*.o
ranlib libmuparser.a
didn't build it dynamically, because it didn't work.

didn't build qhull, took the precompiled version from bin/ in package.
took headers (*.h) from src/libqhull, renamed libqhull.h to qhull.h and changed appropriately #include's in other headers.
getfem++ didn't compile without renaming this header.
didn't build it statically, because it didn't work.

exported variables for compiler and linker:
export CPPFLAGS="-L/home/Xilexio/lib -I/home/Xilexio/include"
export CXXFLAGS="$CPPFLAGS"
LDFLAGS="-L/home/Xilexio/lib"
export LD_LIBRARY_PATH="/home/Xilexio/lib:$LD_LIBRARY_PATH"
export LIBRARY_PATH="C:/GNUstep/msys/1.0/home/Xilexio/lib;$LIBRARY_PATH"
built getfem++:
./configure --enable-qhull --enable-muparser --enable-shared --prefix="/home/xilexio/getfem" && make && make install
then took the libgetfem.a static library, extracted all contents and make a dll out of it:
ar x libgetfem.a
g++ -shared -fPIC -Wl,--enable-auto-import -Wl,-no-undefined -Wl,--enable-runtime-pseudo-reloc -Wl,--out-implib,libgetfem_dll.a -o getfem.dll $CPPFLAGS *.o -lblas -llapack -lqhull -lmuparser

testing by make check with dll was also possible.
moved getfem.dll file into original libgetfem.a file's location (with that extension) and copyied all dlls into that folder.
g++ recognized it as dynamic library and linked with it properly.

each test passed out of the box besides test_large_sliding_contact. this one worked after removing assert:
GMM_ASSERT1(err < 4e-6, "Erroneous gradient of normal vector ");
maybe the reason was lack of high-precision floats library - QD.

note that libgfortran-3 and libquadmath-0 are required for lapack and blas libraries to run. got those from:
http://sourceforge.net/projects/mingw/files/MinGW/Base/gcc/Version4/gcc-4.7.2-1/libgfortran-4.7.2-1-mingw32-dll-3.tar.lzma/download
http://sourceforge.net/projects/mingw/files/MinGW/Base/gcc/Version4/gcc-4.7.2-1/libquadmath-4.7.2-1-mingw32-dll-0.tar.lzma/download
